FROM registry.gitlab.com/sailr/opensource/clarian:kotlin

RUN apk add --update openjdk8

COPY . .

RUN ./gradlew build

EXPOSE 8080

CMD ["./gradlew", "bootRun"]

