package com.jstone28.winch.controllers

import java.util.concurrent.atomic.AtomicLong
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

import com.jstone28.winch.services.KotlinProducer
import com.jstone28.winch.services.Hello

@RestController
@RequestMapping("/api")
class WinchAPIController(private val kotlinProducer: KotlinProducer) {

    private val counter = AtomicLong()

    @PostMapping("/message")
    fun publish(@RequestBody message: String) {
        println(message)
        kotlinProducer.send(message)
    }

    @GetMapping("/hello")
    fun hello(@RequestParam(value = "name", defaultValue = "World") name: String) =
            Hello(counter.incrementAndGet(), "Hello, $name")
}
