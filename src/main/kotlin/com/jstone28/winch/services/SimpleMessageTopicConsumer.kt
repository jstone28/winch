package com.jstone28.winch.services

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
class SimpleMessageTopicConsumer {

    @KafkaListener(topics = ["simple-message-topic"], groupId = "group_id")
    fun processMessage(message: String) {
        println("got message:" + message)
    }

}
