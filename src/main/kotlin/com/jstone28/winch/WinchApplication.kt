package com.jstone28.winch

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class WinchApplication

fun main(args: Array<String>) {
	runApplication<WinchApplication>(*args)
}
